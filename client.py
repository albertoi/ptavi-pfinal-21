#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys
from recv_rtp import RTPHandler
import socketserver
import threading
import time

# Cliente UDP simple.

# Dirección IP y puerto del servidor.

try:
    SERVER = 'localhost'
    dirclient = sys.argv[2].split(":")[1]
    dirservidorRTP = sys.argv[3].split(":")[1]
    fichero = sys.argv[5]
    tiempo = sys.argv[4]
    ip = str(sys.argv[1].split(":")[0])
    PORT = int(sys.argv[1].split(":")[1])
except IndexError:
    sys.exit("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")

def main():

        # Creamos el socket y lo configuramos
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    my_socket.connect((SERVER, PORT))
                    port = my_socket.getsockname()[1]
                    ip = my_socket.getsockname()[0]
                    dir = ip + " " + str(port)
                    print(dir)
                    data1 = "REGISTER" + " " + "sip:" + dirclient + " SIP/2.0" + '\r\n' + dir



                    my_socket.sendto(data1.encode('utf-8'), (SERVER, PORT))
                    # Recibe cinco mensajes
                    data = my_socket.recv(2048)
                    Answer = data.decode("utf-8")
                    answer = Answer.split("\r\n")[0]
                    if answer == "SIP/2.0 200 OK":
                        c = "Content-Type: application/sdp"
                        c1 = "Content-Length:"
                        v = "v = 0"
                        o = "o = " + dirclient + "@"
                        t = "t = 0"
                        m = "m = audio 3543 + RTP"
                        body = c + "\r\n" + c1 + "\r\n" + "\r\n" + v + "\r\n" + o + "\r\n" + "s = mi sesion" + "\r\n" + t + "\r\n" + m
                        data1 = "INVITE" + " " + "sip:" + dirservidorRTP + " SIP/2.0" + '\r\n' + body
                        my_socket.sendto(data1.encode('utf-8'), (SERVER, PORT))
                        data = my_socket.recv(2048)
                        Answer = data.decode("utf-8")
                        answer = Answer.split("\r\n")[0]
                        if answer == "SIP/2.0 200 OK":
                            data1 = "ACK" + " " + "sip:" + dirservidorRTP + " SIP/2.0" + '\r\n'
                            my_socket.sendto(data1.encode('utf-8'), (SERVER, PORT))
                            RTPHandler.open_output('output.mp3')
                            with socketserver.UDPServer((SERVER, 5000), RTPHandler) as serv:
                                print("Listening...")
                                threading.Thread(target=serv.serve_forever).start()
                                time.sleep(30)
                                print("Time passed, shutting down receiver loop.")
                                serv.shutdown()
                            # Cerramos el fichero donde estamos escribiendo los datos recibidos
                            RTPHandler.close_output()


                    print("Recibido:")
                    print(data.decode('utf-8') + ".")
                    print("Terminando programa...")
        except ConnectionRefusedError:
            print("Error ")

if __name__ == "__main__":
    main()
