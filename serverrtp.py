#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""


import socketserver
import sys
import simplertp
import time
import socket

ip = str(sys.argv[1].split(":")[0])
port = int(sys.argv[1].split(":")[1])
audio_file = str(sys.argv[2])
SERVER = "localhost"
cliente = audio_file.split(".")[0] + "@signasong.net"





class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    dicc = {}

    def handle(self):
        data = self.rfile.read().decode("utf-8")
        receptor = data.split()[1].split("sip:")[1]
        userDIC = receptor.split("@")[0]
        ipDIC = receptor.split("@")[1]
        self.dicc["user"] = userDIC
        self.dicc["ip"] = ipDIC
        ipC = self.client_address[0]
        portC = self.client_address[1]

        v = "v = 0"
        o = "o = " + str(receptor)
        t = "t = 0"
        m = "m = audio 3543 + RTP"
        body = "\r\n" + v + "\r\n" + o + "\r\n" + "s = mi sesion" + "\r\n" + t + "\r\n" + m
        print(data.split()[0])

        if data.split()[0] == "ACK":
            print("llego hasta aqui")
            sender = simplertp.RTPSender(ip="localhost", port=5000, file='cancion.mp3', printout=True)
            # Para realizar el envío, llamamos a send_all_thread() de sender,
            # que realizará el envío de paquetes RTP en un hilo (thread) propio
            sender.send_threaded()
            # Commo send_all_thread() ha creado su propio hilo, la ejecución sigue
            # inmediatamente por aquí, y nos podemos quedar esperando 2 segundos
            time.sleep(30)
            # Pasados los tres segundos, finalizamos el envío, tal y como esté en este
            # momento. El hilo (thread) terminará también.
            print("Finalizando el thread de envío.")
            sender.finish()


        elif data.split()[0] == "INVITE":
                print("hay conexion")
                resp = "SIP/2.0 200 OK"
                self.wfile.write(resp.encode("utf-8"))
        elif data.split()[0] == "BYE":
            self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n")
        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed")



def main():
    try:
        serv = socketserver.UDPServer((SERVER, 0), EchoHandler)
        print("Listening...")
        IP = serv.server_address[0]
        PORT = serv.server_address[1]
        dir = IP + " " + str(PORT)
        print(dir)
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                data1 = "REGISTER" + " " + "sip:" + cliente + " SIP/2.0" + '\r\n' + dir

                my_socket.sendto(data1.encode('utf-8'), (SERVER, port))

        except ConnectionRefusedError:
            print("Error ")

    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizando")
        sys.exit(0)


if __name__ == "__main__":
    main()
