#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import socket

PORT = int(sys.argv[1])
SERVER = "localhost"



class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicc = {}

    def register2json(self):

        nameJson = "registered.json"
        with open(nameJson, 'w') as fichero:
            json.dump(self.dicc, fichero, indent=3)

    def json2registered(self):

        nameJson = "registered.json"
        try:
            with open(nameJson, 'r') as fichero:
                self.dicc = json.load(fichero)
        except FileNotFoundError:
            print("Error on registered.json file")


    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        resp = "SIP/2.0 200 OK"
        line = self.rfile.read().decode("utf-8")
        client = line.split(' ')[1].split(':')[1]

        if line.split(' ')[0] == 'REGISTER':
            if len(self.dicc) == 0:
                self.json2registered()
                IP = line.split("\r\n")[1].split(" ")[0]
                PORT = line.split("\r\n")[1].split(" ")[1]
                self.dicc[client] = [IP + ":" + str(PORT)]
                self.register2json()
                self.wfile.write(resp.encode("utf-8"))
        elif line.split(' ')[0] == 'INVITE':
            with open("registered.json", 'r') as fichero:
                self.dicc = json.load(fichero)
            print(self.dicc)
            client = line.split(' ')[1].split(':')[1]
            print(client)
            if client in self.dicc:
               IP = self.dicc[client][0].split(":")[0]
               PUERTO = int(self.dicc[client][0].split(":")[1])
               print(PUERTO)
               print(IP)
               try:
                   with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                       data1 = line
                       my_socket.sendto(data1.encode('utf-8'), (IP, PUERTO))
                       data = my_socket.recv(2048)
                       Answer = data.decode("utf-8")
                       answer = Answer.split("\r\n")[0]
                       if answer == "SIP/2.0 200 OK":
                           self.wfile.write(data)

               except ConnectionRefusedError:
                   print("Error ")

        elif line.split(' ')[0] == 'ACK':
            with open("registered.json", 'r') as fichero:
                self.dicc = json.load(fichero)
            print(self.dicc)
            client = line.split(' ')[1].split(':')[1]
            print(client)
            if client in self.dicc:
                IP = self.dicc[client][0].split(":")[0]
                PUERTO = int(self.dicc[client][0].split(":")[1])
                print(PUERTO)
                print(IP)
                try:
                    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                        data1 = line
                        my_socket.sendto(data1.encode('utf-8'), (IP, PUERTO))

                except ConnectionRefusedError:
                    print("Error ")




def main():
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        serv = socketserver.UDPServer((SERVER, PORT), SIPRegisterHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()  # el servidor va a estar siempre escuchando hasta que reciba una peticion,
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
